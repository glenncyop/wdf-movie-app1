import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms"; 
import { ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "./auth/auth.service";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptorService } from "./auth/token-interceptor.service"

 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesComponent } from './movies/movies.component';
import { CreateComponent } from './create/create.component';
import { CreateMoviesAddComponent } from './create/create-movies-add/create-movies-add.component';
import { CreateMoviesListComponent } from './create/create-movies-list/create-movies-list.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FindPipe } from './find.pipe';
import { MoviesListComponent } from './movies/movies-list/movies-list.component';
import { CreateMoviesDetailComponent } from './create/create-movies-detail/create-movies-detail.component';
import { MovieDetailComponent } from './movies/movie-detail/movie-detail.component';
import { MovieAddComponent } from './movies/movie-add/movie-add.component';
import { LoginComponent } from './login/login.component';
import { GenresComponent } from './genres/genres.component';
import { GenresListComponent } from './genres/genres-list/genres-list.component';
import { GenresAddComponent } from './genres/genres-add/genres-add.component';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    CreateComponent,
    CreateMoviesAddComponent,
    CreateMoviesListComponent,
    HoverHighlightDirective,
    HomeComponent,
    HeaderComponent,
    ContactUsComponent,
    PageNotFoundComponent,
    FindPipe,
    MoviesListComponent,
    CreateMoviesDetailComponent,
    MovieDetailComponent,
    MovieAddComponent,
    LoginComponent,
    GenresComponent,
    GenresListComponent,
    GenresAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule 
  ],
  providers: [AuthService,{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
