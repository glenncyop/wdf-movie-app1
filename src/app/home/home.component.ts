import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  onLogin(username: HTMLInputElement, password: HTMLInputElement) {
    this.authService.login(username.value, password.value)
    .subscribe(
      (res: {token: string}) => {
        console.log(res);
        localStorage.setItem('token', res.token);
        this.router.navigate(["/create-movie"]);
      }, 
      (err) => {
        console.log(err);
        alert("Access Denied");
        this.router.navigate(["/home"]);
      }
    );
  }

}
