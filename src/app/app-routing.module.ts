import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { MoviesComponent } from "./movies/movies.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { CreateComponent } from "./create/create.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { CreateMoviesDetailComponent } from "./create/create-movies-detail/create-movies-detail.component";
import { MovieDetailComponent } from "./movies/movie-detail/movie-detail.component";
import { LoginComponent } from "./login/login.component";
import { GenresComponent } from "./genres/genres.component";

import { AuthGuard } from "./auth/auth.guard";


const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'movies', component: MoviesComponent},
  {path: 'movies-detail/:movie_id', component: MovieDetailComponent},
  {path: 'contact-us', component: ContactUsComponent},
  {path: 'create-movie', canActivate: [AuthGuard], component: CreateComponent},
  {path: 'create-movies-detail/:movie_id', component: CreateMoviesDetailComponent},
  {path: 'create-genre', canActivate: [AuthGuard], component: GenresComponent},
  {path: 'login', component: LoginComponent},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
