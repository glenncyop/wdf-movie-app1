import { Directive, ElementRef, Renderer2, OnInit, HostListener, 
  HostBinding, Input} from '@angular/core';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {
  @HostBinding('style.color') textColor: string;

  @Input('appHoverHighlight')highlightColor: {background: string, text: string};

  constructor(private  elRef: ElementRef, private renderer: Renderer2) {    }

  // ngOnInit() {
  //   this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'yellow');
  // }
  
  @HostListener('mouseenter') mouseOver(evenData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', this.highlightColor.background);
    this.textColor = this.highlightColor.text;
  }

  @HostListener('mouseleave') mouseExit(evenData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.textColor = 'black';
  }

}
