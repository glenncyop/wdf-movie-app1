import { Component, OnInit } from '@angular/core';
import { Movie } from "../movie.model";
import { MoviesService } from "../movies.service"

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {

  //inputInfo : Movie = new Movie(1, "B", "C", "D", "E", "F", "G", "E");

 //@Output() movieAdded = new EventEmitter<Movie>();

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
  }

  // onAddMovie(inputTitle: HTMLInputElement, inputGenre: HTMLInputElement) {
  //   console.log("Add Movie");
  //   //console.log(this.inputInfo);
  //   //console.log(inputTitle.value, inputGenre.value);
  //   console.log(`Title : ${inputTitle.value}`);
  //   console.log(`Genre : ${inputGenre.value}`);

  //   //this.movieAdded.emit(new Movie(inputTitle.value, inputGenre.value)); 
    
  //   this.moviesService.addMovie(new Movie(0, inputTitle.value, inputGenre.value, '', '', '', '', ''));

//  }
}
