export class Movie {
    /*
        public title: string;
        public actors: string;
        public studio: string;
        public date: string;
        public genreid: string;
        public imageurl: string;
    
        constructor(title: string, actors: string, studio: string, date: string, genreid: string, imageurl: string) {
            this.title = title;
            this.actors = actors;
            this.studio = studio;
            this.date = date;
            this.genreid = genreid;
            this.imageurl = imageurl;
        }
    */
    //shortcut
        constructor(
            public movie_id: number, 
            public title: string, 
            public genre: string, 
            public actors: string, 
            public studio: string,
            public date: string, 
            public imageurl: string
            ) {
        }  
    }

    export class MovieDetail {
        constructor(
            public movie_id: number, 
            public title: string, 
            public genre: string, 
            public actors: string, 
            public studio: string,
            public date: string, 
            public imageurl: string
            ) {
        }  
    
    }