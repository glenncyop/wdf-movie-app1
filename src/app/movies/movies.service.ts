import { Injectable, EventEmitter } from '@angular/core';
import { Movie } from "./movie.model";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  movieAdded = new EventEmitter<void>();

  //next_mv_id: number = 8;

  movieList : Movie [] = [
    // new Movie(1, 'Avengers: Infinity War','Action', 'Robert Downey Jr., Chris Evans', 
    // 'Marvel Studios', 'April 22, 2019', '1', 'Twenty-three days after Thanos used the Infinity Gauntlet to disintegrate half of all life in the universe,[N 1] Carol Danvers rescues Tony Stark and Nebula from deep space and returns them to Earth. They reunite with the remaining Avengers—Bruce Banner, Steve Rogers, Thor, Natasha Romanoff, and James Rhodes—and Rocket, and find Thanos on an otherwise uninhabited planet. They plan to retake and use the Infinity Stones to reverse the disintegrations, but Thanos reveals he destroyed them to prevent further use. An enraged Thor decapitates Thanos.'),
    // new Movie(2, 'Toy Story 4', 'Family', 'John_Lasseter, Rashida Jones',
    //  'Walt Disney Studios Motion Pictures', 'June 11, 2019', '4', "During Andy's childhood, R.C. is being swept down a storm drain. Woody leads a successful rescue operation with the other toys, but while they are busy, Bo Peep and several other toys are given away by Andy's younger sister, Molly; despite Woody's efforts to save her, Bo reassures him that part of being a toy is being taken away. Although Woody briefly considers going with her, they bid each other farewell."),
    // new Movie(3, 'Fantastic Beasts', 'Fantasy', 'Eddie Redmayne',
    // 'Warner Bros. Picture', '10 November 2016', '1', 'In 1926, British wizard and "magizoologist" Newt Scamander arrives in New York, en route to Arizona. He encounters Mary Lou Barebone, a non-magical woman ("No-Maj" or "Muggle") who heads the New Salem Philanthropic Society. As Newt listens to her speaking about witches and wizards being real and dangerous, a Niffler escapes from Newt"s magically expanded suitcase, which houses various magical creatures.'),
    // new Movie(4, 'Pokémon Detective Pikachu', 'Adventure', 'Ryan Reynolds',
    // 'Warner Bros. Pictures', 'May 3, 2019', '1', "In the Pokémon universe, Tim Goodman is a 21-year-old insurance salesman who has given up Pokémon training due to the death of his mother and the absence of his father, Harry. He travels to Ryme City – a metropolis where humans and Pokémon live together as equals – to collect Harry's assets following his apparent death in a car crash. "),
    // new Movie(5, 'BrightBurn', 'Horror', 'Elizabeth Banks',
    //  'Sony Pictures Releasing', 'May 24, 2019', '1', "n 2006, Tori and Kyle Breyer are living in Brightburn, Kansas. When a spaceship crashes near their farm with a baby boy inside, the couple adopt him, naming him Brandon and hiding the spaceship in their barn cellar. Twelve years later, the ship begins transmitting an alien message, leading Brandon to discover he has superhuman strength and invulnerability. He sleepwalks to the barn and tries to open the cellar, chanting the craft's message, but Tori intervene"),
    // new Movie(6, 'The Hustle', 'Comedy', 'Anne Hathaway, Rebel Wilson',
    //  'Universal Pictures', 'May 10, 2019', '1', "Two con artists, sophisticated Josephine and abrasive Penny, meet on their way to the French Riviera to find rich men to swindle. They initially work against each other until Penny asks Josephine to take her under her wing, and teach her how to con the way she does. It works at first, but the two then compete to trick a rich tech wiz, Thomas Westerburg."),
    //  new Movie(7, 'Ready Player One', 'Adventure', 'Tye Sheridan, Olivia Cooke',
    //  'Warner Bros. Pictures', 'March 11, 2018', '1', "In 2045, people seek regular escape from life through the virtual reality entertainment universe OASIS (Ontologically Anthropocentric Sensory Immersive Simulation), co-created by James Halliday and Ogden Morrow of Gregarious Games. After Halliday's death, a pre-recorded message left by his avatar Anorak announces a game, granting ownership of OASIS to the first to find the Golden Easter egg within it, which is locked behind a gate requiring three keys. ")  
  ]; 

  constructor(public httpClient: HttpClient) { }

  loadMoviess() {
    return this.httpClient.get<Movie []>("http://localhost:3000/api/movies")
    .pipe(
      map((movies)=> {
        this.movieList = movies;
        return movies;
      },
      (error)=> {console.log(error);})
    )
  }


  // addMovie (newMovieInfo) {
  //   console.log("In Movie Service ... Adding Movie");
  //   newMovieInfo.mv_id = this.next_movie_id++;
  //   this.movieList.push(newMovieInfo);
  //   this.movieAdded.emit();
  // }

  getMovies() {
    console.log("In Movie Service ... Get Movies");
    return this.movieList.slice();
  }

  getMovie(movie_id: number) {
    // for (let movie of this.movieList) {
    //   if (movie.mv_id == mv_id) {
    //     return movie;
    //   }
    // } 
    // return undefined;
    return this.movieList.find(
      movie => { return movie.movie_id == movie_id }, movie_id); 
  }

}
