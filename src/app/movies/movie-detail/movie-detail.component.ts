import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Movie } from "../movie.model"
import { MoviesService } from "../movies.service"

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  selectedMovie: Movie = new Movie (0, "", "", "", "", "", ""); 

  constructor(private moviesService: MoviesService,
    private router : Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.snapshot.params['movie_id']);
    
    this.selectedMovie = this.moviesService.getMovie(
      this.route.snapshot.params['movie_id']
    );
    //console.log(this.selectedMovie);
      if (this.selectedMovie === undefined) {
        this.router.navigate(['not-found']); 
      }
  }

}
