import { Component, OnInit, Input } from '@angular/core';
import { Movie } from "../movie.model";
import { Router } from "@angular/router";

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {

  @Input() myMovies : Movie [];

  searchStrGenre: string = "";
  searchStrTitle: string = "";

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onViewDetail(mv_id: number) {
    this.router.navigate(['/movies-detail', mv_id])
  }

}
