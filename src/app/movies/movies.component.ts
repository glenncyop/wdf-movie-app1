import { Component, OnInit } from '@angular/core';
import { Movie } from './movie.model'
import { MoviesService } from './movies.service'

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
  providers: [MoviesService]
})
export class MoviesComponent implements OnInit {

  movieList : Movie [] = [];
    // new Movie('Avengers: Infinity War','Action'),
    // new Movie('Toy Story 4', 'Family'),
    // new Movie('Fantastic Beasts', 'Fantasy'),
    // new Movie('Pokemon', 'Adventure'),
    // new Movie('BrightBurn', 'Horror'),
    // new Movie('The Hustle', 'Comedy')

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    //this.movieList = this.moviesService.getMovies();
    this.moviesService.loadMoviess()
    .subscribe(()=> {
      this.movieList = this.moviesService.getMovies();
    });

    this.moviesService.movieAdded.subscribe(
      ()=> {
        this.movieList = this.moviesService.getMovies();
      });
  }

  onMovieAdded(newMovieInfo) {
    console.log("In Movie Component, Add a movie");
    this.movieList.push(newMovieInfo);
    console.log(this.movieList); 
  } 
}
