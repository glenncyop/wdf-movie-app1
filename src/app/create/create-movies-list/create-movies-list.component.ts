import { Component, OnInit, Input } from '@angular/core';
import { CreateMovie } from "../create.model"
import { Router } from "@angular/router";
import { CreateMoviesService } from "../create-movies.service";


@Component({
  selector: 'app-create-movies-list',
  templateUrl: './create-movies-list.component.html',
  styleUrls: ['./create-movies-list.component.css']
})
export class CreateMoviesListComponent implements OnInit {

 @Input() myMovies : CreateMovie [];

 searchStr: String = "";

  constructor(private router: Router, private createMoviesService : CreateMoviesService) { }

  ngOnInit() {
  }

  onViewDetail(movie_id: number) {
    this.router.navigate(['/create-movies-detail', movie_id]);
  }
 
  onRemove(movie_id: number) {
    this.createMoviesService.removeMovie(movie_id);
  }
} 
