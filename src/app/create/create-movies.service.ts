import { Injectable, EventEmitter } from '@angular/core';
import { CreateMovie } from "./create.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment"

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class CreateMoviesService {

  movieAdded = new EventEmitter<void>();
  movieListUpdated = new EventEmitter <void> ();

  private movieList : CreateMovie [] = [];

  constructor(public httpClient: HttpClient) { }

  loadMovies() {
    return this.httpClient.get<CreateMovie []>("http://localhost:3000/api/movies")
    .pipe(
      map((movies)=> {
        this.movieList = movies;
        return movies;
      },
      (error)=> {console.log(error);})
    )
  }

  addMovie (newMovieInfo) {
    const httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Cache-Control': 'no-cache'
    });

    const options = {
      headers: httpHeaders
    }
    this.httpClient.post<CreateMovie>("http://localhost:3000/api/movies",
    {info: newMovieInfo}, options)
    .subscribe((respond)=> {
      this.movieList.push(respond);
      this.movieAdded.emit();
    });

    // newMovieInfo.Mv_id = this.next_Mv_id++;
    // console.log("In Create Movie Service.. Adding Movies");
    // this.movieList.push(newMovieInfo);
    // this.movieAdded.emit();
  }


  removeMovie(movie_id) {
    this.httpClient.delete<{success: boolean}>(`${APIEndpoint}/api/movies/${movie_id}}`)
      .subscribe((respond)=> {
        if (respond.success) {
          this.loadMovies().subscribe(()=> {
            this.movieListUpdated.emit();
          })
        }
      }) 
  } 

  getMovies() {
    console.log("In Create Movie Service.. Get Movies");
    return this.movieList.slice();
  }
  getMovie(movie_id: number) {
    // for (let CreateMovie of this.movieList ) {
    //   if (CreateMovie.Mv_id == Mv_id) {
    //     return CreateMovie;
    //   }
    // }
    // return undefined;
    return this.movieList.find (CreateMovie => { return CreateMovie.movie_id == movie_id}, movie_id);
  }
}
