import { Component, OnInit } from '@angular/core';
import { CreateMovie } from "./create.model";
import { CreateMoviesService } from "./create-movies.service"

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [CreateMoviesService]
})
export class CreateComponent implements OnInit {

  movieList : CreateMovie [] = [];
    //new CreateMovie('lol', 'lol', 'lol', '23', '1', 'lol'),
    //new CreateMovie('lol', 'lol', 'lol', '23', '2', 'lol')

  constructor(private createMoviesService: CreateMoviesService) { }

  ngOnInit() {
    //this.movieList = this.createMoviesService.getMovies();
    this.createMoviesService.loadMovies()
      .subscribe(()=>{
        this.movieList = this.createMoviesService.getMovies();
      });

    this.createMoviesService.movieAdded.subscribe(()=>{
      this.movieList = this.createMoviesService.getMovies();
    });
  }

  onAddMovie(newMovieAdded) {
    console.log("In Create Component, add a movie");
    this.movieList.push(newMovieAdded);
    console.log(this.movieList);

  }
 
}
