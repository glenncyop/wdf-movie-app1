import { Component, OnInit} from '@angular/core';
//EventEmitter, Output
import { CreateMovie } from "../create.model";
import { CreateMoviesService } from "../create-movies.service";
import {FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-create-movies-add',
  templateUrl: './create-movies-add.component.html',
  styleUrls: ['./create-movies-add.component.css']
})
export class CreateMoviesAddComponent implements OnInit {

 // inputInfo : Create = new Create ("", "", "", "", "", "");

 // @Output() movieAdded = new EventEmitter<CreateMovie>();
  Form: FormGroup;
  submitted: boolean = false;

  constructor(private createMoviesService: CreateMoviesService) { } 

  ngOnInit() {
    this.Form = new FormGroup ({
      title: new FormControl(null, [this.blankSpaces, Validators.required]),
      actors: new FormControl(null, [this.blankSpaces, Validators.required]),
      studio: new FormControl(null, [this.blankSpaces, Validators.required]),
      date: new FormControl(null, [this.blankSpaces, Validators.required]),
      genre: new FormControl(null, [this.blankSpaces, Validators.required]),
      imageurl: new FormControl(null, [this.blankSpaces, Validators.required])
    }); 
  } 
  
  blankSpaces(control: FormControl): {[s: string]: boolean} {
    if (control.value != null && control.value.trim().length === 0) {
      return {blankSpaces: true}; 
    }
    return null;
  }

  onSubmit() {
    console.log("on Submit");
    console.log(this.Form);
    this.submitted = true;
  }

  reset() {
    this.submitted = false;
  }


  onAddMovie(inputTitle: HTMLInputElement, inputActors: HTMLInputElement, inputStudio: HTMLInputElement,
    inputDate: HTMLInputElement, inputGenre: HTMLInputElement, inputImageurl: HTMLInputElement) {
    console.log("Add Movie");
    //console.log(this.inputInfo);
    //console.log(inputTitle.value, inputActors.value, inputStudio.value, inputDate.value, inputDate.value, inputGenreid.value);

    console.log(`title: ${inputTitle.value}`);
    console.log(`actors: ${inputActors.value}`);
    console.log(`studio: ${inputStudio.value}`);
    console.log(`year: ${inputDate.value}`);
    console.log(`genre: ${inputGenre.value}`);
    console.log(`imageurl: ${inputImageurl.value}`);

    this.createMoviesService.addMovie(new CreateMovie(
      0,
      inputTitle.value,
      inputActors.value,
      inputStudio.value,
      inputDate.value,
      inputGenre.value,
      inputImageurl.value  
    ));

    /*
    this.movieAdded.emit(new CreateMovie(
      inputTitle.value,
      inputActors.value,
      inputStudio.value,
      inputDate.value,
      inputGenreid.value,
      inputImageurl.value
    ));
    */
  }
}
