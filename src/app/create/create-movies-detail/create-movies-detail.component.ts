import { Component, OnInit } from '@angular/core'; 
import { Router, ActivatedRoute } from "@angular/router";
import { CreateMovie } from "../create.model";
import { CreateMoviesService } from "../create-movies.service";

@Component({
  selector: 'app-create-movies-detail',
  templateUrl: './create-movies-detail.component.html',
  styleUrls: ['./create-movies-detail.component.css']
})
export class CreateMoviesDetailComponent implements OnInit {

  selectedMovie: CreateMovie = new CreateMovie(0, "", "", "", "", "", "");

  constructor(private createMoviesService: CreateMoviesService, 
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
   // console.log(this.route.snapshot.params['movie_id']);

    this.selectedMovie = this.createMoviesService.getMovie(this.route.snapshot.params['movie_id']);
    //console.log(this.selectedMovie);
    if (this.selectedMovie === undefined) {
      this.router.navigate(['not-found']);
    }
  } 

}
