import { Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "../../environments/environment"

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

 // loggedIn = false;

  constructor(private router: Router, public  httpClient: HttpClient) { }

  login(username: string, password: string) {
    return this.httpClient.post(`${APIEndpoint}/api/login`,
    {info:{user: username, pwd: password}});
    // this.loggedIn = (username == "glenn" && password == "123");
    // console.log(this.loggedIn);
  
    // if (this.loggedIn) {
    //   this.router.navigate(["/create-movie"]);
    // }
  }
  logout() {
    localStorage.removeItem('token');
   // this.loggedIn = false;
    this.router.navigate(["/"]);
  }

  isLoggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }
 
  // isAuthenticated() {
  //   const promise = new Promise(
  //     (resolve, reject) =>  {
  //       setTimeout(()=>{resolve(this.loggedIn)}, 1000);
  //     }
  //   );
  //   return promise;
  // }
}
